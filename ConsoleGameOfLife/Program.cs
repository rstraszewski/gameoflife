﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleGameOfLife
{
    public class MapClass
    {
        public MapClass()
        {
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    Map[i, j] = 0;
                }
        }

        public int[,] returnMap()
        {
            return Map;
        }
        public int CountLife(int row, int col)
        {
            int Lifes = 0;

            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                {
                    if (!(row + i < 0 || col + j < 0 || col + j > 9 || row + i > 9))
                        Lifes += Map[row + i, col + j];
                }
            Lifes -= Map[row, col];
            return Lifes;
        }

        public void CheckLives()
        {
            int[,] MapNew = new int[10, 10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if(CountLife(i, j)<2 && Map[i,j]==1)
                    {
                        MapNew[i, j] = 0;
                    }
                    if ((CountLife(i, j) == 2 || CountLife(i, j) == 3) && Map[i, j] == 1)
                    {
                        MapNew[i, j] = 1;
                    }
                    if (CountLife(i, j) > 3 && Map[i, j] == 1)
                    {
                        MapNew[i, j] = 0;
                    }
                    if (CountLife(i, j) == 3 && Map[i, j] == 0)
                    {
                        MapNew[i, j] = 1;
                    }

                }
                
            }
            MapOld = Map;
            Map = MapNew;

        }
        public void ShowMap()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.Write(Map[i, j].ToString());
                }
                Console.WriteLine();
            }
        }
        int[,] Map = new int[10, 10];
        int[,] MapOld = new int[10, 10];

        public void Init()
        {
            Random r = new Random();
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {

                    Map[i, j] = r.Next(0, 2);
                }
        }

        public void Run()
        {
            Init();
            ShowMap();

            for (int i = 0; i < 20; i++)
            {
                CheckLives();
                Console.Clear();
                ShowMap();

                Thread.Sleep(1000);

            }


            Console.ReadKey();
        }
    }
    class Program
    {

        static void Main(string[] args)
        {

            MapClass mc = new MapClass();
            mc.Run();
            
        }


    }
}
